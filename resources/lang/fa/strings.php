<?php
return [
    'Orders' => 'سفارشات',
    'Customers' => 'مشتری',
    'Users' => 'کاربر',
    'Owners' => 'مالک',
    'Category' => 'دسته بندی',
    'Estate' => 'ملک',
    'Apartment' => 'آپارتمان',
    'House' => 'خانه ویلایی',
    'Rent' => 'رهن',
    'Sell' => 'فروش',
    'Edit' => 'تغییر',
    'Create' => 'افزودن',
    'Enter' => 'ورود',
    'Name' => 'نام',
    'Cases' => 'موردها',
    'Email' => 'ایمیل',


];
